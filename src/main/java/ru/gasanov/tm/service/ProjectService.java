package ru.gasanov.tm.service;

import ru.gasanov.tm.repository.ProjectRepository;
import ru.gasanov.tm.entity.Project;

import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository repository) {
        this.projectRepository = repository;
    }


    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project create(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public void clear() {
        projectRepository.clear();
    }

    public Project create(String name, String description) {
        if (description == null || description.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project update(Long id, String name, String description) {
        if (description == null || description.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        if (id == null ) return null;
        return projectRepository.update(id, name, description);
    }

    public Project findByIndex(int index) {
        if (index < 0 || index > projectRepository.size() - 1) return null;
        return projectRepository.findByIndex(index);
    }

    public Project findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    public Project removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    public Project removeById(Long id) {
        if (id == 0) return null;
        return projectRepository.removeById(id);
    }

    public Project removeByIndex(int index) {
        if (index < 0 || index > projectRepository.size() - 1) return null;
        return projectRepository.removeByIndex(index);
    }

    public Project findById(Long id) {
        if (id == 0) return null;
        return projectRepository.findById(id);
    }

    public static void main(String[] args) {
        ProjectRepository.main(args);
    }
}
